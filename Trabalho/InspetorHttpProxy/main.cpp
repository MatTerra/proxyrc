#include "mainwindow.h"
#include <QApplication>
#include <QObject>
#include <QThread>
#include <serversocket.h>
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    int port=8228;
    if(argc>2){
        port = atoi(argv[2]);
        qDebug("%d", port);
    }
    MainWindow w;
    w.show();
    w.setPort(port);
    return a.exec();
}
