#ifndef DUMP_H
#define DUMP_H

#include <QWidget>

namespace Ui {
class Dump;
}

class Dump : public QWidget
{
    Q_OBJECT

public:
    explicit Dump(QWidget *parent = nullptr);
    ~Dump();

signals:
    void dumpRequested(QString);

private slots:
    void on_pushButton_clicked();

private:
    Ui::Dump *ui;
};

#endif // DUMP_H
