#include <httputils.h>

httpRequest_t parseRequest(QString request){
    httpRequest_t parsedRequest;
    parsedRequest.payload=request;
    parsedRequest.hostName=getHostName(request);
    parsedRequest.host = getHostAddress(parsedRequest.hostName);
    return parsedRequest;
}


httpRequest_t parseRequest(QString request, int socket){
    httpRequest_t parsedRequest;
    parsedRequest.payload=request;
    parsedRequest.requestSocket = socket;
    parsedRequest.hostName=getHostName(request);
    parsedRequest.host = getHostAddress(parsedRequest.hostName);
    return parsedRequest;
}

QString getHostName(QString request){
    request.replace("http://", "");
    QRegExp hostRX("Host: ([^\r\n:]*)");
    hostRX.indexIn(request, 0);
    QString hostName = hostRX.cap(1);
    qDebug("Host is: <%s>\n\n", hostName.toStdString().c_str());
    return hostName;
}

QString getHostNameFromLink(QString link){
    QRegExp hostRX("((http://)?[^\r\n:/]*)");
    hostRX.indexIn(link, 0);
    QString hostName = hostRX.cap(1);
    qDebug("Host is: <%s>\n\n", hostName.toStdString().c_str());
    return hostName;
}
QString getFileNameFromLink(QString link){
    link.replace("http://", "");
    QRegExp hostRX("[^/]+(/[^\r\n:]*)");
    hostRX.indexIn(link, 0);
    QString fileName = hostRX.cap(1);
    if(fileName.length()==0)
        fileName = "/";
    qDebug("File is: <%s>\n\n", fileName.toStdString().c_str());
    return fileName;
}

struct sockaddr_in getHostAddress(QString hostName){
    struct hostent *he;
    he = gethostbyname(hostName.toStdString().c_str());
    if(he != nullptr){
        struct in_addr **addr_list = reinterpret_cast<struct in_addr **>(he->h_addr_list);
        qDebug("Host Address is:<%s>\n", inet_ntoa(*addr_list[0]));
        struct sockaddr_in serv_addr;
        memset(&serv_addr, '0', sizeof(serv_addr));
        serv_addr.sin_family = AF_INET;
        serv_addr.sin_port = htons(PORTCON);
        if (inet_pton(AF_INET, inet_ntoa(*addr_list[0]), &serv_addr.sin_addr) <= 0){
            perror("\nEndereço Inválido/ Endereço não suportado\n");
            exit(EXIT_FAILURE);
        }
        return serv_addr;
    } else {
        perror("\nEndereço não encontrado\n");
        exit(EXIT_FAILURE);
    }
}

int httputils::connectToRemote(int socket, struct sockaddr* addr, socklen_t flag){
    return connect(socket, addr, flag);
}
