#include <serversocket.h>


ServerSocket::ServerSocket(QObject *_parent, uint16_t port){
    _inPort=port;
    connect(this, &ServerSocket::receivedRequest, this, &ServerSocket::processRequest);
    connect(this, &ServerSocket::requestSent, this, &ServerSocket::processResponse);


}

void ServerSocket::setSocketPort(int port){
    _inPort=port;
    connectSocket();
    qDebug("Socket criado e escutando.\n");
}

int ServerSocket::createSocket(){
    int socketDesc;
    if((socketDesc=socket(AF_INET, SOCK_STREAM, 0))==0){
        perror("Erro ao criar o socket");
        exit(EXIT_FAILURE);
    }
    if(setsockopt(socketDesc, SOL_SOCKET, SO_REUSEADDR,&_opt, sizeof (_opt))){
        perror("Erro ao definir as opções do socket");
        exit(EXIT_FAILURE);
    }
    return socketDesc;
}

void ServerSocket::setTimeout(int socket, int sec){
    timeval tv;
    tv.tv_sec = sec;
    tv.tv_usec = 0;
    if (setsockopt(socket, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv))){
        perror("\nErro ao adicionar timeout\n");
        exit(EXIT_FAILURE);
    }
}

void ServerSocket::connectSocket(){
    _inSocket=createSocket();
    _address.sin_family = AF_INET;
    _address.sin_addr.s_addr = INADDR_ANY;
    _address.sin_port=htons(_inPort);
    if(bind(_inSocket, reinterpret_cast<struct sockaddr*>(& _address), sizeof(_address))<0){
        perror("Erro ao fazer o bind do socket");
        exit(EXIT_FAILURE);
    }
    if (listen(_inSocket, 1) < 0){
        perror("Erro ao iniciar a escuta");
        exit(EXIT_FAILURE);
    }

    qDebug("Socket rodando na porta %d", _inPort);
    emit socketReady();
}

void ServerSocket::listenRequest(){
    int connectionRequest;
    if ((connectionRequest = accept(_inSocket, reinterpret_cast<struct sockaddr*>(& _address) , reinterpret_cast<socklen_t*>(& _addrlen))) < 0) {
        perror("\nErro ao estabelecer a conexão\n");
        exit(EXIT_FAILURE);
    }
    while(_spiderOn);
    qDebug("\nRequest recebido!\n");
    emit receivedRequest(connectionRequest);
}

void ServerSocket::processRequest(int connection){
    qDebug("Conexão estabelecida com %d, lendo mensagem.\n", connection);
    long valread = recv(connection, _buffer, sizeof (_buffer), 0);
    if(valread<0){
        perror("Erro ao ler request");
        exit(EXIT_FAILURE);
    }
    httpRequest_t request = parseRequest(QString(_buffer), connection);
    emit processedRequest(request);
}

std::string ServerSocket::getResponse(httpRequest_t request){
    std::vector<char> buffer(BUF_LENGTH);
    std::string rcv;
    long bytesReceived = 0;
    do {
        bytesReceived = recv(request.responseSocket, &buffer[0], buffer.size(), 0);
        // append string from buffer.
        if ( bytesReceived == -1 ) {
        // error
            qDebug("Connection timed out");
            printf("Errno: %d\n",errno);

        } else {
            rcv.append( buffer.cbegin(), buffer.cend() );
        }
    } while ( bytesReceived > 0 );
    close(request.responseSocket);
    return rcv;
}

void ServerSocket::processResponse(httpRequest_t request){
    std::string rcv=getResponse(request);
    request.payload=QString(rcv.c_str());
    emit processedResponse(request);
}

void ServerSocket::sendRequest(httpRequest_t request){
    setTimeout(request.responseSocket, 30);
    qDebug("Definido timeout.\n");
    qDebug("Host Address is:<%s>\n", inet_ntoa(request.host.sin_addr));
    if (httputils::connectToRemote(request.responseSocket, reinterpret_cast<struct sockaddr*>(& request.host), sizeof(request.host)) < 0){
        perror("\nA conexão remota falhou\n");
        exit(EXIT_FAILURE);
    }
    qDebug("Enviando request: \n%s", request.payload.toStdString().c_str());
    int sent = send(request.responseSocket, request.payload.toStdString().c_str(), strlen(request.payload.toStdString().c_str()), 0);
    printf("%d\n", sent);
}

void ServerSocket::enviarRequest(httpRequest_t request){
    _outSocket = createSocket();
    qDebug("Criado socket externo\n");
    request.responseSocket = _outSocket;
    sendRequest(request);
    emit requestSent(request);
}

void ServerSocket::enviarResponse(httpRequest_t response){
    qDebug("Enviando resposta por %d ao browser. %s", response.requestSocket, response.payload.toStdString().c_str());
    send(response.requestSocket, response.payload.toStdString().c_str(), strlen(response.payload.toStdString().c_str()), 0);
    listenRequest();
}

void ServerSocket::run(){
    listenRequest();
}

void ServerSocket::spider(int level, QString link, QStringList allLinks, int maxLevel){
    emit entryFound(level, link);
    if(allLinks.contains(link))
        return;
    allLinks.append(link);
    if(level==maxLevel){
        if(level==0)
            emit spiderDone();
        return;
    }
    QString host = getHostNameFromLink(link);
    QString file = getFileNameFromLink(link);
    QString requestContent = _defaultRequest;
    QStringList uniqueLinks=QStringList();
    requestContent.replace("$LINK$", file);
    requestContent.replace("$HOST$", host.replace("http://",""));
    qDebug("Parsing request:\n");
    qDebug(requestContent.toUtf8());
    httpRequest_t request = parseRequest(requestContent);
    int _responseSocket = createSocket();
    request.responseSocket = _responseSocket;
    sendRequest(request);
    std::string rcv = getResponse(request);
    qDebug("%s", rcv.c_str());
    qDebug(requestContent.toUtf8());
    QRegExp rx("href[ \t]*=[ \t]*\"(?!https{0,1}:)(?!//)([^\"]*)");
    int pos = rx.indexIn(rcv.c_str());
    while ((pos = rx.indexIn(rcv.c_str(), pos)) != -1) {
        QString i = rx.cap(1);
        qDebug("Link is: %s\n", i.toStdString().c_str());
        if(allLinks.contains(host+i)){
            emit entryFound(level+1, host+i);
        } else {
            uniqueLinks.append(host+i);
        }
        pos += rx.matchedLength();
    }
    foreach(QString uniqueLink, uniqueLinks){

        if(!(uniqueLink.contains(".htm")||uniqueLink.contains(".html")||uniqueLink.contains(".php")||uniqueLink.endsWith("/"))){
            emit entryFound(level+1, uniqueLink);
            allLinks.append(uniqueLink);
        }
        else{
            if(allLinks.contains(uniqueLink)){
                emit entryFound(level+1, uniqueLink);
            } else{
                spider(level+1, uniqueLink, allLinks, maxLevel);
            }
        }

    }
    if(level==0){
        emit spiderDone();
    }

}

void ServerSocket::spiderCreated(){
    _spiderOn=1;
    qDebug("Spider aberto\n");
}

void ServerSocket::initSpider(QString link, int maxLevel){
    qDebug("Spider called with link");
    this->spider(0, link, QStringList(), maxLevel);
}

void ServerSocket::finishSpider(){
    _spiderOn=0;
    qDebug("Spider fechado\n");
}

void ServerSocket::dump(QString link){
    QString host = getHostNameFromLink(link);
    QString fileName = getFileNameFromLink(link);
    QString requestContent = _defaultRequest;
    QStringList uniqueLinks=QStringList();
    requestContent.replace("$LINK$", fileName);
    requestContent.replace("$HOST$", host.replace("http://",""));
    qDebug("Parsing request:\n");
    qDebug(requestContent.toUtf8());
    httpRequest_t request = parseRequest(requestContent);
    int _responseSocket = createSocket();
    request.responseSocket = _responseSocket;
    sendRequest(request);
    std::string rcv = getResponse(request);
    qDebug(rcv.c_str());
    QString page = QString(rcv.c_str());
    QRegExp rx("(<html.*</html>)");
    int pos = rx.indexIn(page);
    QString pg = rx.cap(1);
    qDebug(pg.toUtf8());
    QDir dir("/home/mateusberardo/Documents/Redes/paginaDump");
    if (!dir.exists()){
      dir.mkpath(".");
    }
    QDir dataDir("/home/mateusberardo/Documents/Redes/paginaDump/data");
    if (!dataDir.exists()){
      dataDir.mkpath(".");
    }

    QRegExp rxRsc("src[ \t]*=[ \t]*\"(?!https{0,1}:)([^\"]*)");
    QRegExp fileNameRX("^/(?:.+/)*((?:.+)\.(?:.+))");
    QRegExp fileExtensionRX("(\.(?:.+))");
    pos = rxRsc.indexIn(pg);
    while ((pos = rxRsc.indexIn(pg, pos)) != -1) {
        QString i = rxRsc.cap(1);
        int posFile = fileNameRX.indexIn(i);
        QString dataFileName = rxRsc.cap(1);
        pg.replace(i, dataDir.path()+"/"+dataFileName);
        int posExt = fileExtensionRX.indexIn(dataFileName);

        QString requestContent = _defaultMediaRequest.replace("$LINK$", i).replace("$HOST$", host);
        httpRequest_t request = parseRequest(requestContent);
        int _responseSocket = createSocket();
        request.responseSocket = _responseSocket;
        sendRequest(request);
        std::string rcv = getResponse(request);

//        QString mediaFile = QString(dataDir.path()+"/"+dataFileName);
//        FILE *file = fopen(mediaFile.toStdString().c_str(), "wb+ ");
//        fwrite(rcv.c_str() , rcv.size() , 1, file);
//        fclose(file);

        pos += rx.matchedLength();
    }






    QFile file("/home/mateusberardo/Documents/Redes/paginaDump/pagina.html");
    if(!file.open(QIODevice::WriteOnly)){
        qDebug("Erro ao abrir o arquivo");
        file.close();
    } else {
            qDebug("Escrevendo o arquivo");
            file.write(pg.toUtf8());
            file.close();
    }
    qDebug("savedFile");

//    int pos = rx.indexIn(rcv.c_str());
//    while ((pos = rx.indexIn(rcv.c_str(), pos)) != -1) {
//        QString i = rx.cap(1);
//        qDebug("Link is: %s\n", i.toStdString().c_str());
//        if(allLinks.contains(host+i)){
//            emit entryFound(level+1, host+i);
//        } else {
//            uniqueLinks.append(host+i);
//        }
//        pos += rx.matchedLength();
//    }
//    foreach(QString uniqueLink, uniqueLinks){

//        if(!(uniqueLink.contains(".htm")||uniqueLink.contains(".html")||uniqueLink.contains(".php")||uniqueLink.endsWith("/"))){
//            emit entryFound(level+1, uniqueLink);
//            allLinks.append(uniqueLink);
//        }
//        else{
//            if(allLinks.contains(uniqueLink)){
//                emit entryFound(level+1, uniqueLink);
//            } else{
//                spider(level+1, uniqueLink, allLinks, maxLevel);
//            }
//        }

//    }
//    if(level==0){
//        emit spiderDone();
//    }

}
