#include "spider.h"
#include "ui_spider.h"

Spider::Spider(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Spider){
    ui->setupUi(this);
    requestEdit = ui->spiderView;
    SpiderDone =this->findChild<QCheckBox *>("DoneSpider");
}

Spider::~Spider(){
    delete ui;
}

void Spider::closeEvent(QCloseEvent *event){
    qDebug("closed Spider\n");
    emit closedSpider();
    event->accept();
}

void Spider::addEntry(int level, QString link){
    requestEdit->moveCursor(QTextCursor::End);
    if(link.length()==0)
        return;
    for(;level>0;level--){
        requestEdit->insertPlainText(QString('\t'));
    }
    requestEdit->insertPlainText(link);
    requestEdit->insertPlainText(QString('\n'));
}

void Spider::on_genSpiderBtn_clicked()
{
    QLineEdit *linkEdit = this->findChild<QLineEdit *>("urlEdit");
    QString link = linkEdit->text();
    QSpinBox *maxLevelEdit = this->findChild<QSpinBox *>("maxLevelEdit");
    int maxLevel = maxLevelEdit->value();
    requestEdit->clear();
    SpiderDone->setChecked(false);
    emit requestedSpider(link, maxLevel);
}

void Spider::spiderFinished(){
    SpiderDone->setChecked(true);
}
