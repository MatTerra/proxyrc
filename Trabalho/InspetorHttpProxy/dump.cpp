#include "dump.h"
#include "ui_dump.h"

Dump::Dump(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Dump)
{
    ui->setupUi(this);
}

Dump::~Dump()
{
    delete ui;
}

void Dump::on_pushButton_clicked(){
    QLineEdit *url = ui->urlEdit;
    QString link = url->text();
    emit dumpRequested(link);
}
