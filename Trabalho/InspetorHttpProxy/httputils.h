#ifndef HTTPUTILS_H
#define HTTPUTILS_H

#include <netinet/in.h>
#include <QString>
#include <QRegExp>
#include <netdb.h>
#include <arpa/inet.h>
#include <QMetaType>

#define PORTCON 80
typedef struct{
    struct sockaddr_in host;
    int requestSocket;
    int responseSocket;
    QString payload;
    QString hostName;
} httpRequest_t;
Q_DECLARE_METATYPE(httpRequest_t)
httpRequest_t parseRequest(QString);
httpRequest_t parseRequest(QString, int);
QString getHostName(QString);
QString getHostNameFromLink(QString);
QString getFileNameFromLink(QString);
struct sockaddr_in getHostAddress(QString);

namespace httputils {
    int connectToRemote(int, struct sockaddr*, socklen_t);
}

#endif // HTTPUTILS_H
