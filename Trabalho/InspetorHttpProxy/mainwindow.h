#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTextEdit>
#include <httputils.h>
#include "serversocket.h"
#include "spider.h"
#include "dump.h"
#include <QObject>
#include <QDebug>
#include <QThread>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr, int port = 8228);
    ServerSocket *server;
    void setPort(int);
    ~MainWindow();

signals:
    void requestPronto(httpRequest_t);
    void responsePronta(httpRequest_t);
    void spiderCreated();
    void DumpRequested();

public slots:
    void displayRequest(httpRequest_t);
    void displayResponse(httpRequest_t);

private slots:
    void on_LiberarCom_clicked();

    void on_pushButton_clicked();

    void on_Spider_clicked();

    void on_Dump_clicked();

private:
    Ui::MainWindow *ui;
    httpRequest_t request;
    int _port;
    QThread* thread;
};

#endif // MAINWINDOW_H
