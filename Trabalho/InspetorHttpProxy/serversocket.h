#ifndef SERVERSOCKET_H
#define SERVERSOCKET_H

#define BUF_LENGTH 1
#define PORT 8228

#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <QObject>
#include <QString>
#include <httputils.h>
#include <iostream>
#include <QThread>
#include <unistd.h>
#include <QList>
#include <QMetaType>
#include <QStringList>
#include <QDir>
#include <QTextStream>
#include <QFile>



class ServerSocket : public QThread{
    Q_OBJECT
public:
    explicit ServerSocket(QObject *_parent=nullptr, uint16_t port=PORT);
    void connectSocket(),
        run(),
        spider(int, QString, QStringList, int),
        setTimeout(int, int);
    int createSocket();
    void setSocketPort(int);
    void sendRequest(httpRequest_t);
    std::string getResponse(httpRequest_t);


public slots:
    void processRequest(int);
    void processResponse(httpRequest_t);
    void enviarRequest(httpRequest_t);
    void enviarResponse(httpRequest_t);
    void listenRequest();
    void initSpider(QString, int);
    void finishSpider();
    void spiderCreated();
    void dump(QString);

signals:
    void receivedRequest(int);
    void processedRequest(httpRequest_t);
    void processedResponse(httpRequest_t);
    void requestSent(httpRequest_t);
    void socketReady();
    void entryFound(int, QString);
    void spiderDone();
private:
    uint16_t _inPort=8228, _outPort=80;
    int _opt=1, _inSocket, _outSocket, _addrlen = sizeof(_address), _spiderOn=0;
    struct sockaddr_in _address;
    char _buffer[8192]={0};
    QString _defaultRequest = "GET $LINK$ HTTP/1.1\r\nHost: $HOST$\r\nAccept: text/html;\r\n\r\n";
    QString _defaultMediaRequest = "GET $LINK$ HTTP/1.1\r\nHost: $HOST$\r\n\r\n";
};

#endif // SERVERSOCKET_H
