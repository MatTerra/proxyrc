#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent, int port) :
    QMainWindow(parent),
    ui(new Ui::MainWindow){
    ui->setupUi(this);
    qRegisterMetaType<httpRequest_t>("HttpRequest");
    server = new ServerSocket(this);
    thread = new QThread();
     // move the task object to the thread BEFORE connecting any signal/slots
    server->moveToThread(thread);
    connect(server, &ServerSocket::processedRequest, this, &MainWindow::displayRequest);
    connect(this, &MainWindow::requestPronto, server, &ServerSocket::enviarRequest);
    connect(server, &ServerSocket::processedResponse, this, &MainWindow::displayResponse);
    connect(this, &MainWindow::responsePronta, server, &ServerSocket::enviarResponse);
    connect(this, &MainWindow::spiderCreated, server, &ServerSocket::spiderCreated);

}

MainWindow::~MainWindow(){
    delete ui;
}

void MainWindow::setPort(int port){
    _port=port;
    server->setSocketPort(port);
    server->start();
    thread->start();
}

void MainWindow::displayRequest(httpRequest_t request){
    QTextEdit* requestEdit = ui->centralWidget->findChild<QTextEdit *>("textIn");
    MainWindow::request=request;
    requestEdit->setText(MainWindow::request.payload.replace("\n","\\n").replace("\r","\\r"));
    requestEdit->setReadOnly(false);
}

void MainWindow::displayResponse(httpRequest_t response){
    QTextEdit* responseEdit = ui->centralWidget->findChild<QTextEdit *>("textOut");
    MainWindow::request=response;
    responseEdit->setText(MainWindow::request.payload.replace("\n","\\n").replace("\r","\\r"));
    responseEdit->setReadOnly(false);
}

void MainWindow::on_LiberarCom_clicked(){
    QTextEdit* requestEdit = ui->centralWidget->findChild<QTextEdit *>("textIn");
    requestEdit->setReadOnly(true);
    request.payload = requestEdit->toPlainText().replace("\\n","\n").replace("\\r","\r");
    emit requestPronto(request);
}

void MainWindow::on_pushButton_clicked(){
    QTextEdit* responseEdit = ui->centralWidget->findChild<QTextEdit *>("textOut");
    responseEdit->setReadOnly(true);
    request.payload = responseEdit->toPlainText().replace("\\n","\n").replace("\\r","\r");
    emit responsePronta(request);
    server->start();
}

void MainWindow::on_Spider_clicked(){
    Spider *spider = new Spider();
    spider->setAttribute(Qt::WA_DeleteOnClose);
    emit spiderCreated();
    connect(spider, &Spider::requestedSpider, server, &ServerSocket::initSpider);
    connect(spider, &Spider::closedSpider, server, &ServerSocket::finishSpider);
    connect(server, &ServerSocket::entryFound, spider, &Spider::addEntry);
    connect(server, &ServerSocket::spiderDone, spider, &Spider::spiderFinished);
    spider->show();
}

void MainWindow::on_Dump_clicked(){
    Dump *dump = new Dump();
    dump->setAttribute(Qt::WA_DeleteOnClose);
    connect(dump, &Dump::dumpRequested, server, &ServerSocket::dump);
    dump->show();
}
