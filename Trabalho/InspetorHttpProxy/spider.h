#ifndef SPIDER_H
#define SPIDER_H

#include <QWidget>
#include <QTextEdit>
#include <QLineEdit>
#include <QCloseEvent>
#include <QSpinBox>
#include <QCheckBox>
#include <QTextCursor>

namespace Ui {
class Spider;
}

class Spider : public QWidget
{
    Q_OBJECT

public:
    explicit Spider(QWidget *parent = nullptr);
    ~Spider();
    void closeEvent(QCloseEvent *);
public slots:
    void addEntry(int, QString);
    void spiderFinished();

signals:
    void requestedSpider(QString, int);
    void closedSpider();

private slots:
    void on_genSpiderBtn_clicked();

private:
    Ui::Spider *ui;
    QTextEdit* requestEdit;
    QCheckBox* SpiderDone;
};

#endif // SPIDER_H
