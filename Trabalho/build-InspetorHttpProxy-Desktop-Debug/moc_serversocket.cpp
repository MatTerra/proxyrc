/****************************************************************************
** Meta object code from reading C++ file 'serversocket.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../InspetorHttpProxy/serversocket.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'serversocket.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_ServerSocket_t {
    QByteArrayData data[19];
    char stringdata0[242];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ServerSocket_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ServerSocket_t qt_meta_stringdata_ServerSocket = {
    {
QT_MOC_LITERAL(0, 0, 12), // "ServerSocket"
QT_MOC_LITERAL(1, 13, 15), // "receivedRequest"
QT_MOC_LITERAL(2, 29, 0), // ""
QT_MOC_LITERAL(3, 30, 16), // "processedRequest"
QT_MOC_LITERAL(4, 47, 13), // "httpRequest_t"
QT_MOC_LITERAL(5, 61, 17), // "processedResponse"
QT_MOC_LITERAL(6, 79, 11), // "requestSent"
QT_MOC_LITERAL(7, 91, 11), // "socketReady"
QT_MOC_LITERAL(8, 103, 10), // "entryFound"
QT_MOC_LITERAL(9, 114, 10), // "spiderDone"
QT_MOC_LITERAL(10, 125, 14), // "processRequest"
QT_MOC_LITERAL(11, 140, 15), // "processResponse"
QT_MOC_LITERAL(12, 156, 13), // "enviarRequest"
QT_MOC_LITERAL(13, 170, 14), // "enviarResponse"
QT_MOC_LITERAL(14, 185, 13), // "listenRequest"
QT_MOC_LITERAL(15, 199, 10), // "initSpider"
QT_MOC_LITERAL(16, 210, 12), // "finishSpider"
QT_MOC_LITERAL(17, 223, 13), // "spiderCreated"
QT_MOC_LITERAL(18, 237, 4) // "dump"

    },
    "ServerSocket\0receivedRequest\0\0"
    "processedRequest\0httpRequest_t\0"
    "processedResponse\0requestSent\0socketReady\0"
    "entryFound\0spiderDone\0processRequest\0"
    "processResponse\0enviarRequest\0"
    "enviarResponse\0listenRequest\0initSpider\0"
    "finishSpider\0spiderCreated\0dump"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ServerSocket[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       7,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   94,    2, 0x06 /* Public */,
       3,    1,   97,    2, 0x06 /* Public */,
       5,    1,  100,    2, 0x06 /* Public */,
       6,    1,  103,    2, 0x06 /* Public */,
       7,    0,  106,    2, 0x06 /* Public */,
       8,    2,  107,    2, 0x06 /* Public */,
       9,    0,  112,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      10,    1,  113,    2, 0x0a /* Public */,
      11,    1,  116,    2, 0x0a /* Public */,
      12,    1,  119,    2, 0x0a /* Public */,
      13,    1,  122,    2, 0x0a /* Public */,
      14,    0,  125,    2, 0x0a /* Public */,
      15,    2,  126,    2, 0x0a /* Public */,
      16,    0,  131,    2, 0x0a /* Public */,
      17,    0,  132,    2, 0x0a /* Public */,
      18,    1,  133,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, 0x80000000 | 4,    2,
    QMetaType::Void, 0x80000000 | 4,    2,
    QMetaType::Void, 0x80000000 | 4,    2,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::QString,    2,    2,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, 0x80000000 | 4,    2,
    QMetaType::Void, 0x80000000 | 4,    2,
    QMetaType::Void, 0x80000000 | 4,    2,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::Int,    2,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    2,

       0        // eod
};

void ServerSocket::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<ServerSocket *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->receivedRequest((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->processedRequest((*reinterpret_cast< httpRequest_t(*)>(_a[1]))); break;
        case 2: _t->processedResponse((*reinterpret_cast< httpRequest_t(*)>(_a[1]))); break;
        case 3: _t->requestSent((*reinterpret_cast< httpRequest_t(*)>(_a[1]))); break;
        case 4: _t->socketReady(); break;
        case 5: _t->entryFound((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 6: _t->spiderDone(); break;
        case 7: _t->processRequest((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->processResponse((*reinterpret_cast< httpRequest_t(*)>(_a[1]))); break;
        case 9: _t->enviarRequest((*reinterpret_cast< httpRequest_t(*)>(_a[1]))); break;
        case 10: _t->enviarResponse((*reinterpret_cast< httpRequest_t(*)>(_a[1]))); break;
        case 11: _t->listenRequest(); break;
        case 12: _t->initSpider((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 13: _t->finishSpider(); break;
        case 14: _t->spiderCreated(); break;
        case 15: _t->dump((*reinterpret_cast< QString(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< httpRequest_t >(); break;
            }
            break;
        case 2:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< httpRequest_t >(); break;
            }
            break;
        case 3:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< httpRequest_t >(); break;
            }
            break;
        case 8:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< httpRequest_t >(); break;
            }
            break;
        case 9:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< httpRequest_t >(); break;
            }
            break;
        case 10:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< httpRequest_t >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (ServerSocket::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ServerSocket::receivedRequest)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (ServerSocket::*)(httpRequest_t );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ServerSocket::processedRequest)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (ServerSocket::*)(httpRequest_t );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ServerSocket::processedResponse)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (ServerSocket::*)(httpRequest_t );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ServerSocket::requestSent)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (ServerSocket::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ServerSocket::socketReady)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (ServerSocket::*)(int , QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ServerSocket::entryFound)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (ServerSocket::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ServerSocket::spiderDone)) {
                *result = 6;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject ServerSocket::staticMetaObject = { {
    &QThread::staticMetaObject,
    qt_meta_stringdata_ServerSocket.data,
    qt_meta_data_ServerSocket,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *ServerSocket::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ServerSocket::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_ServerSocket.stringdata0))
        return static_cast<void*>(this);
    return QThread::qt_metacast(_clname);
}

int ServerSocket::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QThread::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    }
    return _id;
}

// SIGNAL 0
void ServerSocket::receivedRequest(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void ServerSocket::processedRequest(httpRequest_t _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void ServerSocket::processedResponse(httpRequest_t _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void ServerSocket::requestSent(httpRequest_t _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void ServerSocket::socketReady()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void ServerSocket::entryFound(int _t1, QString _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void ServerSocket::spiderDone()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
