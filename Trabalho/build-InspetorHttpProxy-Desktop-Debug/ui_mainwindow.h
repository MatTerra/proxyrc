/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *LiberarCom;
    QPushButton *Spider;
    QPushButton *Dump;
    QPushButton *pushButton;
    QHBoxLayout *horizontalLayout_3;
    QTextEdit *textIn;
    QTextEdit *textOut;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(400, 300);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        LiberarCom = new QPushButton(centralWidget);
        LiberarCom->setObjectName(QString::fromUtf8("LiberarCom"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(1);
        sizePolicy.setVerticalStretch(1);
        sizePolicy.setHeightForWidth(LiberarCom->sizePolicy().hasHeightForWidth());
        LiberarCom->setSizePolicy(sizePolicy);

        horizontalLayout_2->addWidget(LiberarCom);

        Spider = new QPushButton(centralWidget);
        Spider->setObjectName(QString::fromUtf8("Spider"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(1);
        sizePolicy1.setHeightForWidth(Spider->sizePolicy().hasHeightForWidth());
        Spider->setSizePolicy(sizePolicy1);

        horizontalLayout_2->addWidget(Spider);

        Dump = new QPushButton(centralWidget);
        Dump->setObjectName(QString::fromUtf8("Dump"));
        sizePolicy1.setHeightForWidth(Dump->sizePolicy().hasHeightForWidth());
        Dump->setSizePolicy(sizePolicy1);

        horizontalLayout_2->addWidget(Dump);

        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        sizePolicy.setHeightForWidth(pushButton->sizePolicy().hasHeightForWidth());
        pushButton->setSizePolicy(sizePolicy);

        horizontalLayout_2->addWidget(pushButton);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        textIn = new QTextEdit(centralWidget);
        textIn->setObjectName(QString::fromUtf8("textIn"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy2.setHorizontalStretch(1);
        sizePolicy2.setVerticalStretch(1);
        sizePolicy2.setHeightForWidth(textIn->sizePolicy().hasHeightForWidth());
        textIn->setSizePolicy(sizePolicy2);

        horizontalLayout_3->addWidget(textIn);

        textOut = new QTextEdit(centralWidget);
        textOut->setObjectName(QString::fromUtf8("textOut"));
        sizePolicy2.setHeightForWidth(textOut->sizePolicy().hasHeightForWidth());
        textOut->setSizePolicy(sizePolicy2);

        horizontalLayout_3->addWidget(textOut);


        verticalLayout->addLayout(horizontalLayout_3);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 400, 23));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        LiberarCom->setText(QApplication::translate("MainWindow", "Envia Request", nullptr));
        Spider->setText(QApplication::translate("MainWindow", "Spider", nullptr));
        Dump->setText(QApplication::translate("MainWindow", "Dump", nullptr));
        pushButton->setText(QApplication::translate("MainWindow", "Libera Resposta", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
