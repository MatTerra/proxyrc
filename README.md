# Proxy Server
Esse trabalho consiste em um servidor de proxy que possa editar o request e gerar a árvore de arquivos da resposta.

## Compilando e executando
Para compilar, acesse a pasta examples e rode o comando: ```g++ -std=c++0x ServerCorrigido.cpp -o server```.
Para executar, na mesma pasta da compilação, rode o comando ```./server```.

#### Referências
http://www.linuxhowtos.org/C_C++/socket.htm
https://www.binarytides.com/receive-full-data-with-recv-socket-function-in-c/
http://www.linuxhowtos.org/manpages/2/socket.htm
https://www.gta.ufrj.br/ensino/eel878/sockets/sockaddr_inman.html
https://www.ibm.com/support/knowledgecenter/en/SSLTBW_2.3.0/com.ibm.zos.v2r3.bpxbd00/rtrea.htm
https://www.gnu.org/software/libc/manual/html_node/Host-Names.html
https://beej.us/guide/bgnet/html/multi/gethostbynameman.html
https://github.com/klarkgable/Projeto-TR2-1-2018/blob/master/Header.cpp
http://www.dicas-l.com.br/arquivo/programando_socket_em_c++_sem_segredo.php#.XQKTrnVKi00
http://pubs.opengroup.org/onlinepubs/009695399/functions/recv.html
https://renenyffenegger.ch/notes/development/languages/C-C-plus-plus/CPP-Standard-Library/sstream/ostringstream

