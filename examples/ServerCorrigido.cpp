// Server side C/C++ program to demonstrate Socket programming 
#include <unistd.h> 


#include <regex>
#include <string>

#include <errno.h>



#include <stdio.h> 
#include <sys/socket.h> 
#include <stdlib.h> 
#include <netinet/in.h> 
#include <netdb.h>
#include <arpa/inet.h>
#include <iostream>

#define PORT 8228 
#define PORTCON 80

struct hostent *he;
struct in_addr a;


int main(int argc, char const* argv[]){
	int server_fd, new_socket, new_socket2, valread;
	struct sockaddr_in address;
	int opt = 1;
	int addrlen = sizeof(address);
	char buffer[8192] = { 0 };
	char* hello = "Hello from server";
	#define CHUNK_SIZE 1
	char chunk[CHUNK_SIZE];
	int total_size=0, size_recv=0;

	// Creating socket descriptor 
	if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0){
		perror("socket failed");
		exit(EXIT_FAILURE);
	}

	// Set the socket options
	if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR,
		&opt, sizeof(opt))){
		perror("setsockopt");
		exit(EXIT_FAILURE);
	}
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = INADDR_ANY;
	address.sin_port = htons(PORT);

	// Forcefully attaching socket to the selected port 
	if (bind(server_fd, (struct sockaddr*) & address,
		sizeof(address)) < 0){
		perror("bind failed");
		exit(EXIT_FAILURE);
	}
	if (listen(server_fd, 3) < 0){
		perror("listen");
		exit(EXIT_FAILURE);
	}
	while(true){
		struct in_addr **addr_list;
		struct sockaddr_in serv_addr;
		if ((new_socket = accept(server_fd, (struct sockaddr*) & address,
			(socklen_t*)& addrlen)) < 0) {//Blocked until connect is called in the client
			perror("accept");
			exit(EXIT_FAILURE);
		}

		printf("Server accepted connection, reading message\n");
		valread = recv(new_socket, buffer, 8192, 0); //Blocked until message is sent from the client		
		printf("%s\n", buffer);
		std::string result;
		try {
		  	std::string subject(buffer);
		  	std::regex re("Host: (.*)");
		  	std::smatch match;
		  	if (std::regex_search(subject, match, re) && match.size() > 1) {
		    		result = match.str(1);
		  	} else {
		    		result = std::string("");
		  	} 
		} catch (std::regex_error& e) {
		  	// Syntax error in the regular expression
		}
		
		printf("Host is: <%s>\n", result.c_str());
		he = gethostbyname(result.c_str());
		if(he != NULL){	
			addr_list = (struct in_addr **)he->h_addr_list;
			printf("<%s>\n", inet_ntoa(*addr_list[0]));
			struct sockaddr_in address;
			int sock = 0, valread;
			struct sockaddr_in serv_addr;
			
			

			memset(&serv_addr, '0', sizeof(serv_addr));

			serv_addr.sin_family = AF_INET;
			serv_addr.sin_port = htons(PORTCON);
			if (inet_pton(AF_INET, inet_ntoa(*addr_list[0]), &serv_addr.sin_addr) <= 0){
				printf("\nInvalid address/ Address not supported \n");
				return -1;
			}
			if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0){
				printf("\n Socket creation error \n");
				return -1;
			}
			timeval tv;
			tv.tv_sec = 5;
			tv.tv_usec = 0;
			if (setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv))){
				perror("setsockopt");
				exit(EXIT_FAILURE);
			}
			if (connect(sock, (struct sockaddr*) & serv_addr, sizeof(serv_addr)) < 0){
				printf("\nConnection Failed \n");
				return -1;
			}
			printf("Client: Sending Hello\n");
			send(sock, buffer, strlen(buffer), 0);
			//valread = read(sock, buffer, 8192); //Blocked until message is sent from the server
			/*std::ostringstream oss;
			while(1){
				if((size_recv =  recv(sock , chunk , CHUNK_SIZE , 0) ) < 1){
					printf("a");
					printf("%d", errno);
					break;
				}
				else{
					oss << chunk[0];
					std::cout<<chunk[0];
				}
			}
			*/
			const unsigned int MAX_BUF_LENGTH = 1;
			std::vector<char> buffer(MAX_BUF_LENGTH);
			std::string rcv;   
			int bytesReceived = 0;
			do {
			    bytesReceived = recv(sock, &buffer[0], buffer.size(), 0);
			    // append string from buffer.
			    if ( bytesReceived == -1 ) { 
				// error 
			    } else {
				rcv.append( buffer.cbegin(), buffer.cend() );
			    }
			} while ( bytesReceived > 0 );
			//printf("%s\n", oss.str().c_str());				
			send(new_socket, rcv.c_str(), rcv.size(), 0);
			close(sock);
			close(new_socket)			
			printf("%s\n", buffer);

		}
		//while (*he->h_addr_list){
		//	bcopy(*he->h_addr_list++, (char *) &a, sizeof(a));
		//	printf("address: %s\n", inet_ntoa(a));
		//}
		//printf("Server: Sending Hello\n");
	}
	return 0;
}
