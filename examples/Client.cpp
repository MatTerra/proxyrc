// Client side C/C++ program to demonstrate Socket programming 
#include <stdio.h> 
#include <sys/socket.h> 
#include <stdlib.h> 
#include <netinet/in.h> 
#include <string.h> 
#include <unistd.h>
#include <arpa/inet.h>

#define PORT 80 

int main(int argc, char const* argv[]){
	struct sockaddr_in address;
	int sock = 0, valread;
	int n;
	struct sockaddr_in serv_addr;
	char* hello = "GET http://www.linuxhowtos.org/manpages/2/socket.htm HTTP/1.1\r\n";
	//char* hello2 = "Host: linuxhowtos.org\r\n\r\n";
	char* buffer[1024]={0};
	if ((sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0){
		printf("\n Socket creation error \n");
		return -1;
	}

	memset(&serv_addr, '0', sizeof(serv_addr));

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(PORT);

	// Convert IPv4 and IPv6 addresses from text to binary form 
	if (inet_pton(AF_INET, "178.63.75.8", &serv_addr.sin_addr) <= 0){
		printf("\nInvalid address/ Address not supported \n");
		return -1;
	}

	if (connect(sock, (struct sockaddr*) & serv_addr, sizeof(serv_addr)) < 0){
		printf("\nConnection Failed \n");
		return -1;
	}
	
	send(sock, hello, strlen(hello), 0);
	printf("Client: Sending Hello\n");
	//send(sock, hello2, strlen(hello2), 0);
	valread = read(sock, buffer, 1024); //Blocked until message is sent from the server
	/*while(1){
		if((n =  recv(sock , buffer , 2 , 0)) < 0){
			printf("a");
			break;
		}
		else{
			printf("b");
			printf("%c" , *buffer);
		}
		printf("c");
	}*/	
	printf("%s", buffer);
	return 0;
}
